package DAO;

public class GnlUserMobileAccount {

	private long mUserMobileAccountId;
	private int mCountryCode;
    private long mMobileNumber;
   
    private int mDeviceOsType;
    private int mVerificationCode;
    private String mVerificationTextMessage;
    
	public int getmCountryCode() {
		return mCountryCode;
	}
	public void setmCountryCode(int mCountryCode) {
		this.mCountryCode = mCountryCode;
	}
	public long getmMobileNumber() {
		return mMobileNumber;
	}
	public void setmMobileNumber(long mMobileNumber) {
		this.mMobileNumber = mMobileNumber;
	}
	public long getmUserMobileAccountId() {
		return mUserMobileAccountId;
	}
	public void setmUserMobileAccountId(long mUserMobileAccountId) {
		this.mUserMobileAccountId = mUserMobileAccountId;
	}
	public int getmDeviceOsType() {
		return mDeviceOsType;
	}
	public void setmDeviceOsType(int mDeviceOSType) {
		this.mDeviceOsType = mDeviceOSType;
	}
	public int getmVerificationCode() {
		return mVerificationCode;
	}
	public void setmVerificationCode(int mVerificationCode) {
		this.mVerificationCode = mVerificationCode;
	}
	public String getmVerificationTextMessage() {
		return mVerificationTextMessage;
	}
	public void setmVerificationTextMessage(String mVerificationTextMessage) {
		this.mVerificationTextMessage = mVerificationTextMessage;
	}
	
	
	
}
