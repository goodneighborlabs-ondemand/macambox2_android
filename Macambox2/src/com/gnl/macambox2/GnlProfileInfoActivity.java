package com.gnl.macambox2;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.actionbarsherlock.app.SherlockActivity;

public class GnlProfileInfoActivity extends SherlockActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gnl_profile_info);
	}

	@Override
	public void onBackPressed() {
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (Context.NOTIFICATION_SERVICE != null) {
			Log.d("MB", "Going to vanish notifications");
			String ns = Context.NOTIFICATION_SERVICE;
			NotificationManager nMgr = (NotificationManager) getApplicationContext()
					.getSystemService(ns);
			nMgr.cancelAll();
		}
	}

}
