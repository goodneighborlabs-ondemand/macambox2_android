package com.gnl.macambox2;

import helpers.GnlConstants;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsMessage;
import android.util.Log;

import com.gnl.macambox2.R;

/**
 * @author Piotr Malak 
 * Receiver to capture sms message containing specified
 *         text.
 * 
 */
public class GnlSmsReceiver extends BroadcastReceiver {

	static Context mContextc;
	@Override
	public void onReceive(Context context, Intent intent) {
		
		mContextc=context;

		Log.i("MB", "GnlSmsReceiver onReceive");
		
		if (intent.getAction()
				.equals("android.provider.Telephony.SMS_RECEIVED")) {

			Bundle pudsBundle = intent.getExtras();
			if (pudsBundle == null) {
				Log.e("MB", "pudsBundle is null");
				return;
			}
			Object[] pdus = (Object[]) pudsBundle.get("pdus");
			if (pdus == null || pdus.length == 0) {
				Log.e("MB", "puds is null or 0");
				return;
			}
			SmsMessage messages = SmsMessage.createFromPdu((byte[]) pdus[0]);
			
			if(messages== null){
				Log.e("MB", "messages are null");
				return;
			}
			
			if (messages.getMessageBody().contains(context.getString(R.string.enter_phone_screen_message_to_send_by_sms))) {
				
				
				abortBroadcast();
				//context.unregisterReceiver(this);
				Log.i("MB","SMS captured: "+ messages.getMessageBody());
				
				//Verify phone number on server using received code
				//Get the code from sms
				final String stringCode = messages.getMessageBody().substring(messages.getMessageBody().length()-6);
				int verificationCode = -1;
				try{
					verificationCode = Integer.valueOf(stringCode);
				}
				catch(NumberFormatException nfe ){
				
					Log.e("MB","Problem converting code "+stringCode+", exception: "+nfe.getLocalizedMessage());
					return;
				}
				
				//Check if code contains numbers only
				if(verificationCode > -1)
				{
					//send the code to the server
					//We need to run all network operations from separate thread since Android 4.0
					new Thread(new Runnable() { 
					      
						@Override
						public void run() {
							if(GnlMacamboxServerConnector.GetInstance().verifyNumberUsingCode(stringCode)){
							Message msg = Message.obtain(uiHandler);	
							 msg.arg1 = IS_VERIFIED;
					        uiHandler.sendMessage(msg);
							}else{
								
								//Server problem
							}
						}
					}).start();
					
				}else{
					Log.e("MB","Problem code is less than 0");
				}
				
				
				
			}
			Log.i("MB","SMS body: "+ messages.getMessageBody());
		}
	}
	
//--------------------------------------------------------------------------------------------------------
	//Handler for capture after thread operations on UI thread
	final static int IS_VERIFIED = 10;
	final int IS_NOT_VERIFIED = 20;
	static private Handler uiHandler = new Handler(){
	
	 
	    @Override
	    public void handleMessage(Message msg) {
	        // a message is received; update UI text view
	    	 super.handleMessage(msg);
	    	 
	    	 
	    	 if(msg.arg1 == IS_VERIFIED){
	    		 
	    		//Shared preferences
					SharedPreferences preferences = PreferenceManager
							.getDefaultSharedPreferences(mContextc);
					//Set isVerified in Shared prefs
					
					SharedPreferences.Editor editor = preferences.edit();

					editor.putBoolean(GnlConstants.SHARED_PREF_IS_USER_VERIFIED,
							true);

					editor.commit();
					
					
					//Navigate to Profile info screen if application is visible to the user only
					boolean mIsApplicationActive =preferences.getBoolean(GnlConstants.SHARED_PREF_IS_APPLICATION_ACTIVE, false );
					
					if (mIsApplicationActive) {
						Intent nIntent = new Intent(mContextc,
								GnlProfileInfoActivity.class);
						nIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
						mContextc.startActivity(nIntent);
					}else
					{
						//display notification informing user that verification is done
						showNotification(mContextc);
					}
	    	 
	    	 }
	    	 
	}
	};
//---------------------------------------------------------------------------------------------------------	
	// creating and showing system notification.
	static void showNotification(Context context){

		   
		   Intent intent=new Intent(context,GnlProfileInfoActivity.class);
		   PendingIntent  pending=PendingIntent.getActivity(context, 0, intent, 0);
		   
		   NotificationCompat.Builder mBuilder =
	               new NotificationCompat.Builder(context)
		   			//.setLargeIcon(userBitmap)
	               .setSmallIcon(R.drawable.ic_launcher)
	               .setContentTitle(context.getString(R.string.sms_receiver_notification_title))
	               .setContentIntent(pending)
	               .setTicker(context.getString(R.string.sms_receiver_notification_body))
	               .setContentText(context.getString(R.string.sms_receiver_notification_body))
	               .setAutoCancel(true);
	
	       
	       Notification notification = mBuilder.build();
	       notification.defaults = Notification.DEFAULT_SOUND|Notification.DEFAULT_LIGHTS;
	       
	        
	       
	       NotificationManager notificationManager = (NotificationManager)
	    		   context.getApplicationContext().getSystemService(context.NOTIFICATION_SERVICE); 
	      
	      
	       notification.flags |= Notification.FLAG_AUTO_CANCEL;
	       notificationManager.notify(0, notification);
	
	    }

}
