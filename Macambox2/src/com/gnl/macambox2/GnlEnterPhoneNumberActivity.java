package com.gnl.macambox2;

import java.lang.ref.WeakReference;

import helpers.GnlConstants;
import helpers.GnlDialogActionInterface;
import helpers.GnlDialogHelper;
import DAO.GnlUserMobileAccount;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.Spinner;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

/**
 * @author Piotr Malak Controller class for
 * 
 */
public class GnlEnterPhoneNumberActivity extends SherlockActivity {

	ActionBar mActionBar;
	public MenuItem mBtnDone; // this is public for testing purposes
	EditText mPhoneNumberEt, mCountryCodeEt;
	Spinner mCountryNameSpn;

	// -------------------------------------------------------------------------------
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gnl_enter_phone_number);

		// ActionBar Sherlock initialization
		mActionBar = getSupportActionBar();
		mActionBar.setTitle(R.string.enter_phone_screen_title);

		// Widgets ascription
		mPhoneNumberEt = (EditText) findViewById(R.id.etPhoneNumberEnterPhoneNumber);
		mCountryCodeEt = (EditText) findViewById(R.id.etCountryCodeEnterPhoneNumber);
		mCountryNameSpn = (Spinner) findViewById(R.id.spnCountryNameEnterPhoneNumber);

		// Listener for phone number text changes.
		mPhoneNumberEt.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

				if (s.length() > 0) {
					if (mBtnDone != null) {
						mBtnDone.setEnabled(true);
					}
				} else {
					if (mBtnDone != null) {
						mBtnDone.setEnabled(false);
					}
				}

			}

			@Override
			public void afterTextChanged(Editable s) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}
		});
	}

	// ----------------------------------------------------------------------------
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		mBtnDone = menu.add(Menu.NONE, menuItemDone, Menu.NONE,
				R.string.enter_phone_screen_btn_done_label);
		mBtnDone.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

		if (mPhoneNumberEt != null && mPhoneNumberEt.getText().length() < 1)
			mBtnDone.setEnabled(false);

		return true;
	}

	// -------------------------------------------------------------------------------
	final int menuItemDone = 10;

	// -------------------------------------------------------------------------------
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case menuItemDone:

			doneAction();

			return true;

		}

		return super.onOptionsItemSelected(item);

	}

	// -------------------------------------------------------------------------------

	/**
	 * Action to be done after touching done button.
	 */
	void doneAction() {

		// Validate imput
		if (validatePhoneNumber()) {
			// Display confirmation dialog
			displayPhoneConfirmation();
		}
	}

	// -------------------------------------------------------------------------------------
	

	/**
	 * Validate phone number entered by user
	 * 
	 * @return isNumber valid
	 */
	private boolean validatePhoneNumber() {

		boolean mRes = false;
		String mEnteredNumber = removeNonNumbers(mPhoneNumberEt.getText()
				.toString());

		if (mEnteredNumber == null) {
			return mRes;
		}

		int mLengthOfEntry = mEnteredNumber.length();

		if (mLengthOfEntry > GnlConstants.VALID_LENGTH) {
			displayNumberTooLong();
		} else if (mLengthOfEntry < GnlConstants.VALID_LENGTH) {
			displayNumberTooShort();
		} else if (mLengthOfEntry == GnlConstants.VALID_LENGTH) {

			mRes = true;
		}

		return mRes;
	}

	// ---------------------------------------------------------------------------------------

	/**
	 * removes non number characters from the string
	 * 
	 * @param rawNumber
	 *            - String potentially containing non numbers characters
	 * @return - String containing numbers only
	 */
	private String removeNonNumbers(String rawNumber) {

		if (rawNumber != null) {

			return rawNumber.replaceAll("[^\\d]", "");
		}

		else {
			return null;
		}

	}

	// -------------------------------------------------------------------------------------
	/**
	 * Displays warning dialog number entered is too short
	 */
	private void displayNumberTooShort() {

		GnlDialogHelper
				.getInstance()
				.ShowMessage(
						this,
						getString(R.string.enter_phone_screen_dialog_too_short_label)
								+ " "
								+ mCountryNameSpn.getSelectedItem().toString(),
						null,
						getString(R.string.enter_phone_screen_dialog_too_short_button_caption));

	}

	// -------------------------------------------------------------------------------------
	/**
	 * Displays warning dialog number entered is too long
	 */
	private void displayNumberTooLong() {

		GnlDialogHelper
				.getInstance()
				.ShowMessage(
						this,
						getString(R.string.enter_phone_screen_dialog_too_long_label)
								+ " "
								+ mCountryNameSpn.getSelectedItem().toString(),
						null,
						getString(R.string.enter_phone_screen_dialog_too_long_button_caption));

	}

	// -------------------------------------------------------------------------------------
	/**
	 * Display dialog to confirm phone number entered
	 */
	private void displayPhoneConfirmation() {

		String mPhoneNrToConfirm = mPhoneNumberEt.getText().toString();
		mPhoneNrToConfirm = mPhoneNrToConfirm.substring(0, 3) + "-"
				+ mPhoneNrToConfirm.substring(3, 6) + "-"
				+ mPhoneNrToConfirm.substring(6);

		GnlDialogHelper
				.getInstance()
				.ShowMessage(
						this,
						getString(R.string.enter_phone_screen_dialog_confirm_number_label)
								+ "\n" + mPhoneNrToConfirm,
						getString(R.string.enter_phone_screen_dialog_confirm_number_button_ok_caption),
						getString(R.string.enter_phone_screen_dialog_confirm_number_button_edit_caption),
						new DialogCallbackHelper());

	}

	// -------------------------------------------------------------------------------------
	/**
	 * Sends phone confirmation code request to the Macambox Server
	 */
	private void registerPhoneNumberOnServer() {

		int mCountryCode;
		long mPhoneNumber;

		try { // to convert strings from edit texts to numbers

			if (mCountryCodeEt.getText().toString().contains("+")) {
				mCountryCode = Integer.valueOf(mCountryCodeEt.getText()
						.toString().substring(1));
			} else {
				mCountryCode = Integer.valueOf(mCountryCodeEt.getText()
						.toString());
			}

			mPhoneNumber = Long.valueOf(mPhoneNumberEt.getText().toString());

		} catch (NumberFormatException nfe) {

			Log.e("MB",
					"Error converting string to number:"
							+ nfe.getLocalizedMessage());
			return;

		} catch (IndexOutOfBoundsException iobe) {

			Log.e("MB", "Error cutting string:" + iobe.getLocalizedMessage());
			return;

		}

		final GnlUserMobileAccount mUserMobileAccount = new GnlUserMobileAccount();
		mUserMobileAccount.setmCountryCode(mCountryCode);
		mUserMobileAccount.setmMobileNumber(mPhoneNumber);
		mUserMobileAccount
				.setmVerificationTextMessage(getString(R.string.enter_phone_screen_message_to_send_by_sms));
		mUserMobileAccount
				.setmDeviceOsType(GnlConstants.DEVICE_OS_TYPE_ANDROID);

		// We need to run all network operations from separate thread since
		// Android 4.0
		new Thread(new Runnable() {

			@Override
			public void run() {
				if (GnlMacamboxServerConnector.GetInstance()
						.registerUserPhoneNumber(mUserMobileAccount)) {

					Message msg = Message.obtain(uiHandler);
					msg.arg1 = IS_REGISTERED;
					uiHandler.sendMessage(msg);
				} else {

					// Server problem
				}
			}
		}).start();
		
		// Save provided phone number in shared preferences
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(GnlEnterPhoneNumberActivity.this);
		SharedPreferences.Editor editor = preferences.edit();

		editor.putLong(GnlConstants.SHARED_PREF_MY_PHONE_NUMBER,
				Long.valueOf(mPhoneNumberEt.getText().toString()));

		editor.commit();
		
		navigateToSmsVerificationActivity();
	}

	// ---------------------------------------------------------------------------------------------
	// Handler for capture after thread operations on UI thread
	final int IS_REGISTERED = 10;

	Handler uiHandler = new Handler(new Handler.Callback() {

		@Override
		public boolean handleMessage(Message msg) {

			if (msg.arg1 == IS_REGISTERED) {

			
			}

			return false;

		}
	});

	// class Handler{
	//
	// @Override
	// public void handleMessage(Message msg) {
	// // a message is received; update UI text view
	// super.handleMessage(msg);
	//
	// if (msg.arg1 == IS_REGISTERED) {
	//
	// navigateToSmsVerificationActivity();
	//
	// // Save provided phone number in shared preferences
	// SharedPreferences preferences = PreferenceManager
	// .getDefaultSharedPreferences(GnlEnterPhoneNumberActivity.this);
	// SharedPreferences.Editor editor = preferences.edit();
	//
	// editor.putLong(GnlConstants.SHARED_PREF_MY_PHONE_NUMBER,
	// Long.valueOf(mPhoneNumberEt.getText().toString()));
	//
	// editor.commit();
	//
	// }
	//
	// }
	// };
	// --------------------------------------------------------------------------------------------
	// Getting Dialog responses for confirmation dialog.

	public class DialogCallbackHelper implements GnlDialogActionInterface {

		@Override
		public void PositiveAction() {

			registerPhoneNumberOnServer();

		}

		@Override
		public void NegativeAction() {
			// we don't use negative action apart from dismissing dialog.
		}

	}

	// ----------------------------------------------------------------------------
	void navigateToSmsVerificationActivity() {

		Intent intent = new Intent(GnlEnterPhoneNumberActivity.this,
				GnlSmsVerificationActivity.class);

		
		GnlEnterPhoneNumberActivity.this.startActivity(intent);

	}

	// ----------------------------------------------------------------------------
	/**
	 * Saves in shared preferences current state of application.
	 * 
	 * @param isActive
	 *            - current state of application
	 */
	void saveApplicationState(boolean isActive) {

		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		SharedPreferences.Editor editor = preferences.edit();

		editor.putBoolean(GnlConstants.SHARED_PREF_IS_APPLICATION_ACTIVE,
				isActive);

		editor.commit();
	}

	// -------------------------------------------------------------------
	@Override
	protected void onResume() {
		super.onResume();
		saveApplicationState(true);
		// if verified navigate to profile
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		boolean mIsUserVerified = preferences.getBoolean(
				GnlConstants.SHARED_PREF_IS_USER_VERIFIED, false);
		if (mIsUserVerified) {
			Intent intent = new Intent(GnlEnterPhoneNumberActivity.this,
					GnlProfileInfoActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
					| Intent.FLAG_ACTIVITY_CLEAR_TOP);
			GnlEnterPhoneNumberActivity.this.startActivity(intent);
		}
	}

	// ------------
	@Override
	protected void onPause() {
		super.onPause();

		saveApplicationState(false);

	}
	// --------------------------------------------------------------------------------------------------------

	// --------------------------------------------------------------------------------------------

}
