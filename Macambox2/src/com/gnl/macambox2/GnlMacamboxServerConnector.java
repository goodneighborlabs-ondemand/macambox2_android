package com.gnl.macambox2;

import helpers.GnlConstants;
import helpers.GnlMacamboxServerHelper;
import DAO.GnlUserMobileAccount;
import android.util.Log;

import com.google.gson.Gson;

/**
 * @author Piotr Malak Class for communication with Macambox Server
 * 
 */
public class GnlMacamboxServerConnector extends GnlMacamboxServerHelper {

	static GnlMacamboxServerConnector sInstance;

	// ----------------------------------------------------------------------------
	/**
	 * Returns singleton instance of the class
	 * 
	 * @return instance of the class
	 */
	public static GnlMacamboxServerConnector GetInstance() {

		if (sInstance == null)
			sInstance = new GnlMacamboxServerConnector();

		return sInstance;
	}

	/**
	 * Sending send sms request to the server
	 * 
	 * @param userMobileAccount
	 *            instance of GnlUserMobileAccount DAO class
	 */
	public boolean registerUserPhoneNumber(
			final GnlUserMobileAccount userMobileAccount) {

		if (userMobileAccount == null) {
			return false;
		}
		Log.d("MB", "Send request");

		Gson gson = new Gson();
		String mJson = gson.toJson(userMobileAccount);
		
		if(mJson == null)
			return false;

		Log.d("MB",
				"Sending sms request for number:"
						+ userMobileAccount.getmCountryCode()
						+ userMobileAccount.getmMobileNumber());
		String response = PostData(GnlConstants.SERVER_URL_VERIFY_USER, mJson);

		Log.i("MB", "Response form server: " + response);

		if (response.startsWith("ERROR")) {
			Log.e("MB", "Problem sending sms:" + response);
			return false;
		}

		return true;
	}

	// -------------------------------------------------------------------------------------------
	/**
	 * Sending number verification request to the server
	 * 
	 * @param code
	 *            - verification code received by sms
	 * @return
	 */
	synchronized public boolean verifyNumberUsingCode(final String code) {

		if (code == null)
			return false;

		Log.d("MB", "Sending code to verify:" + code);
		String response = getData(GnlConstants.SERVER_URL_VERIFY_USER, code);

		Log.i("MB", "Response form server: " + response);

		if (response.startsWith("ERROR")) {
			Log.e("MB", "Problem verifying:" + response);
			return false;

		}

		return true;

	}

	// ------------------------------------------------------------------------------
	public boolean MethodToTest() {

		return true;
	}

}
