package com.gnl.macambox2;

import helpers.GnlConstants;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;

public class GnlSmsVerificationActivity extends SherlockActivity {

	boolean isReceiverRegistered = false;
	TextView tvPhoneNumberProvided;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gnl_sms_verification);

		tvPhoneNumberProvided = (TextView) findViewById(R.id.tvNumberProvidedSmsVerification);

		setNumberProvided();

		// registerSmsReceiver();
	}

	// ----------------------------------------------------------------------------------------
	private void setNumberProvided() {

		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(this);

		String mPhoneNrToConfirm = String.valueOf(preferences.getLong(
				GnlConstants.SHARED_PREF_MY_PHONE_NUMBER, -1));
		if (mPhoneNrToConfirm.length() > 7) {
			mPhoneNrToConfirm = mPhoneNrToConfirm.substring(0, 3) + "-"
					+ mPhoneNrToConfirm.substring(3, 6) + "-"
					+ mPhoneNrToConfirm.substring(6);
		}

		tvPhoneNumberProvided.setText(mPhoneNrToConfirm);
	}

	// -----------------------------------------------------------------------------------------
	public void navigateToPhoneNumber(View v) {
		finish();
	}

	// --------------------------------------------------------------------------------------------

	@Override
	protected void onResume() {
		super.onResume();
		saveApplicationState(true);
		// if verified navigate to profile
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		boolean mIsUserVerified = preferences.getBoolean(
				GnlConstants.SHARED_PREF_IS_USER_VERIFIED, false);
		if (mIsUserVerified) {
			Intent intent = new Intent(this, GnlProfileInfoActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
					| Intent.FLAG_ACTIVITY_CLEAR_TOP);
			this.startActivity(intent);
		}
	}

	// ------------
	@Override
	protected void onPause() {
		super.onPause();

		saveApplicationState(false);

	}

	// ----------------------------------------------------------------------------

	/**
	 * Saves in shared preferences current state of application.
	 * 
	 * @param isActive
	 *            - current state of application
	 */
	void saveApplicationState(boolean isActive) {

		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		SharedPreferences.Editor editor = preferences.edit();

		editor.putBoolean(GnlConstants.SHARED_PREF_IS_APPLICATION_ACTIVE,
				isActive);

		editor.commit();
	}

}
