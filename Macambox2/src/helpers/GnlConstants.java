package helpers;

/**
 * @author Piotr Malak
 * Holds all static final data for the application
 *
 */
public class GnlConstants {
	
	//GnlMacamboxServerConnector class
	static final String JBOSS_SERVER_HOST_DEV = "50.112.93.139";
	static String sJBossServerHost =  JBOSS_SERVER_HOST_DEV;
	public static final String SERVER_URL_VERIFY_USER = "http://"+sJBossServerHost+":8080/Macambox2Service/macambox2/userMobileAccount";
	
	 /// Device OS types
    public static final int DEVICE_OS_TYPE_IOS = 1;
    public static final int DEVICE_OS_TYPE_ANDROID = 2;
    
    //SharedPreferences Key Names
    public static final String SHARED_PREF_MY_PHONE_NUMBER = "myPhoneNumber";
    public static final String SHARED_PREF_IS_APPLICATION_ACTIVE = "isApplicationActive";
    public static final String SHARED_PREF_IS_USER_VERIFIED = "isVerified";
    
    //Length of telephone number
    public static final int VALID_LENGTH = 10;
}
