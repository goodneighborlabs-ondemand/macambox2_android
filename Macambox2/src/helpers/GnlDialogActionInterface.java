package helpers;

public interface GnlDialogActionInterface {
	
	public void PositiveAction();
	public void NegativeAction();

}
