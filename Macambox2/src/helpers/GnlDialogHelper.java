package helpers;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * @author Piotr Malak This one is for displaying alert messages with Message
 *         and two optional buttons
 * 
 */
public class GnlDialogHelper {

	static GnlDialogHelper sInstance;
	String mMessageToShow;
	String mPositiveButtonCaption;
	String mNegativeButtonCaption;
	Context mContext;
	GnlDialogActionInterface mResultAction;

	public static GnlDialogHelper getInstance() {
		if (sInstance == null)
			sInstance = new GnlDialogHelper();

		return sInstance;
	}

	/**
	 * Displays alert dialog with message and two optional buttons. Passing
	 * caption of particular button as null hides the button. Negative button
	 * dismisses dialog.
	 * 
	 * @param c
	 *            - Context of calling method
	 * @param message
	 *            - alert message
	 * @param positiveButtonCaption
	 *            - positive button caption
	 * @param negativeButtonCaption
	 *            - negative button caption
	 * @param resultAction - interface to send back result of dialog
	 *
	 */
	public void ShowMessage(Context c, String message,
			String positiveButtonCaption, String negativeButtonCaption,
			GnlDialogActionInterface resultAction) {

		mMessageToShow = message;
		mPositiveButtonCaption = positiveButtonCaption;
		mNegativeButtonCaption = negativeButtonCaption;
		mContext = c;
		mResultAction = resultAction;

		createDialog().show();

	}
	
	/**
	 * Displays alert dialog with message and two optional buttons. Passing
	 * caption of particular button as null hides the button. Negative button
	 * dismisses dialog.
	 * 
	 * @param c
	 *            - Context of calling method
	 * @param message
	 *            - alert message
	 * @param positiveButtonCaption
	 *            - positive button caption
	 * @param negativeButtonCaption
	 *            - negative button caption
	 * 
	 */
	public void ShowMessage(Context c, String message,
			String positiveButtonCaption, String negativeButtonCaption) {
		
		ShowMessage(c,message, positiveButtonCaption, negativeButtonCaption, null);
	
	}

	// ----------------------------------------------------------------------------
	/**
	 * Creates AlertDialog.
	 * 
	 * @return AlertDIalog ready to show
	 */
	AlertDialog createDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

		builder.setMessage(mMessageToShow);
		
		
		if (mPositiveButtonCaption != null
				&& mPositiveButtonCaption.length() > 0)
			// Add positive button
			builder.setPositiveButton(mPositiveButtonCaption,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							// User clicked OK button
							if(mResultAction!=null)
							{
								mResultAction.PositiveAction();
							}
							
							dialog.dismiss();
						}
					});

		if (mNegativeButtonCaption != null
				&& mNegativeButtonCaption.length() > 0)
			// Add negative button
			builder.setNegativeButton(mNegativeButtonCaption,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							
							if(mResultAction!=null)
							{
								mResultAction.NegativeAction();
							}
							dialog.dismiss();
						}
					});

		// Create the AlertDialog
		return builder.create();
	}

}
