package helpers;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import android.util.Log;

/**
 * @author Piotr Malak Helper for communicate with Macambox Server on HTTP level
 * 
 */
public class GnlMacamboxServerHelper {

	static int CONNECTION_TIMEOUT = 5000;
	static int DATARETRIEVAL_TIMEOUT = 5000;

	// -----------------------------------------------------------------------------------------------------

	/**
	 * Sending post request to the server
	 * 
	 * @param url
	 *            - address of web method
	 * @param json
	 *            - json object
	 * @return http result
	 */
	protected synchronized String PostData(String url, String json) {
		
		//Security check
		if(url==null || json == null)
			return "ERROR no input values";

		String mRet = "ERROR no response";
		HttpParams mParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(mParams, CONNECTION_TIMEOUT);
		HttpConnectionParams.setSoTimeout(mParams, DATARETRIEVAL_TIMEOUT);
		HttpClient httpclient = new DefaultHttpClient(mParams);

		try {

			HttpPost httppost = new HttpPost(url.toString());
			httppost.setHeader("Content-type", "application/json");

			StringEntity se = new StringEntity(json, "UTF8");
			se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
					"application/json"));
			httppost.setEntity(se);

			HttpResponse mResponse = httpclient.execute(httppost);

			String mTemp;
			if (mResponse.getEntity() != null)
				mTemp = EntityUtils.toString(mResponse.getEntity(), HTTP.UTF_8);
			else
				mTemp = "Http code:"
						+ mResponse.getStatusLine().getStatusCode();

			mRet = mTemp;

		} catch (ClientProtocolException e) {

			Log.e("MB",
					"ClientProtocolException problem: "
							+ e.getLocalizedMessage());
			mRet = "ERROR " + e.getLocalizedMessage();

		} catch (IOException e) {
			Log.e("MB", "IOException problem: " + e.getLocalizedMessage());
			mRet = "ERROR" + e.getLocalizedMessage();
		} catch (Exception e) {
			Log.e("MB", "Common problem: " + e.getLocalizedMessage());
			mRet = "ERROR " + e.getLocalizedMessage();
		}

		return mRet;
	}
	
	//--------------------------------------------------------------------------------------------	
		/**
		 * Sending get method with attached parameter
		 * @param url - address of web method
		 * @param code - parameter to attach to the url
		 * @return result
		 */
	protected synchronized String getData(String url,String code) {
		    
			//Security check
			if(url==null || code == null)
				return "ERROR no input values";
			
			// Create a new HttpClient and Post Header
			
			String mRet = "ERROR no response";
		    HttpParams myParams = new BasicHttpParams();
		    HttpConnectionParams.setConnectionTimeout(myParams, CONNECTION_TIMEOUT);
		    HttpConnectionParams.setSoTimeout(myParams, DATARETRIEVAL_TIMEOUT);
		    HttpClient httpclient = new DefaultHttpClient(myParams);
		    
		    
		    try {

		    	HttpGet httpget = new HttpGet(url.toString()+"/"+code);
		        httpget.setHeader("Content-type", "application/json");
		        
		        
		        
		        HttpResponse response = httpclient.execute(httpget);
		        String temp;
		        if(response.getEntity()!=null)
		        	temp = EntityUtils.toString(response.getEntity(), HTTP.UTF_8);
		        else
		        	temp = "Http code:"+response.getStatusLine().getStatusCode();
		        
		        Log.i("MB", "getData status code:"+response.getStatusLine().getStatusCode());
		        
		        if(response.getStatusLine().getStatusCode()>204)
		        {
		        	mRet = "ERROR http code:" +response.getStatusLine().getStatusCode();
		        }
		        
		        else{
		        	mRet = temp;
		        }

		    } catch (ClientProtocolException e) {
		    	Log.e("MB", "ClientProtocolException problem: " + e.getLocalizedMessage());
		    	mRet = "ERROR" + e.getLocalizedMessage();

		    } catch (IOException e) {
		    	Log.e("MB", "IOException problem: " + e.getLocalizedMessage());
		    	mRet = "ERROR" + e.getLocalizedMessage();
		    }
		    catch (Exception e) {
				Log.e("MB", "Common problem: " + e.getLocalizedMessage());
				mRet = "ERROR " + e.getLocalizedMessage();
		    }
		   
		    return mRet;
		}
	// --------------------------------------------------------------------------------------------
	
}
