package com.gnl.macambox2.test;

import junit.framework.TestCase;

import DAO.GnlUserMobileAccount;

import com.gnl.macambox2.GnlMacamboxServerConnector;

public class GnlMacamboxServerConnectorTest extends TestCase {

	GnlMacamboxServerConnector mClassUnderTest;

	protected void setUp() throws Exception {
		super.setUp();

		mClassUnderTest = GnlMacamboxServerConnector.GetInstance();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testSendNullSms() {

		assertFalse(mClassUnderTest.registerUserPhoneNumber(null));

	}

	// if GnlUserMobileAccount object exists the registerUserPhoneNumber method
		// should be performed though
		// improper values - server accepts all kind of data .
	public void testSendSmsWithNoPhoneNumber() {

		GnlUserMobileAccount mSms = new GnlUserMobileAccount();
		mSms.setmCountryCode(48);

		mSms.setmVerificationTextMessage("I do not have phone nr");

		assertTrue(mClassUnderTest.registerUserPhoneNumber(mSms));

	}

	// if GnlUserMobileAccount object exists the registerUserPhoneNumber method
		// should be performed though
		// improper values .
	public void testSendSmsWithNoValuesSet() {

		GnlUserMobileAccount mSms = new GnlUserMobileAccount();

		assertTrue(mClassUnderTest.registerUserPhoneNumber(mSms));

	}

	// if GnlUserMobileAccount object exists the registerUserPhoneNumber method
	// should be performed though
	// improper values .
	public void testSendSmsWithMinusValuesSet() {

		GnlUserMobileAccount mSms = new GnlUserMobileAccount();
		mSms.setmCountryCode(-4443);
		mSms.setmMobileNumber(-823793286);

		assertTrue(mClassUnderTest.registerUserPhoneNumber(mSms));

	}

	// test verifying number
	//null passed to verifying method - should return false
	public void testVerifyNumberUsingCodeWithNull() {

		assertFalse(mClassUnderTest.verifyNumberUsingCode(null));
	}
	//letters passed to verifying method - should return false
	public void testVerifyNumberUsingCodeWithLetters() {

		assertFalse(mClassUnderTest.verifyNumberUsingCode("ABCDXYZ"));
	}

	
	//Server returns true for every numeric value sent
	public void testVerifyNumberUsingRandomNumbers() {

		assertTrue(mClassUnderTest.verifyNumberUsingCode("123456"));
	}

}
