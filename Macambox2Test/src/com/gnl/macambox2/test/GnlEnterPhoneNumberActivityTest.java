package com.gnl.macambox2.test;

import java.util.Random;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.widget.EditText;

import com.gnl.macambox2.GnlEnterPhoneNumberActivity;

/**
 * @author Piotr Malak Main test class for activity GnlEnterPhoneNumberActivity
 */
public class GnlEnterPhoneNumberActivityTest extends
		ActivityInstrumentationTestCase2<GnlEnterPhoneNumberActivity> {

	GnlEnterPhoneNumberActivity mActivity;

	final int NUMBER_LENGTH = 9;

	public GnlEnterPhoneNumberActivityTest() {
		super(GnlEnterPhoneNumberActivity.class);

	}

	// public Macambox2Test() {
	// super("com.gnl.macambox2", GnlEnterPhoneNumberActivity.class);
	//
	// }
	//

	@Override
	protected void setUp() throws Exception {
		super.setUp();

		// Turning off touch mode in the device
		setActivityInitialTouchMode(false);

		mActivity = (GnlEnterPhoneNumberActivity) getActivity();

	}

	// ------------------------------------------------------------------
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	// ------------------------------------------------------------------

	/**
	 * Simulates enter number of length specify by NUMBER_LENGTH variable
	 * 
	 * @throws Throwable
	 */
	public void testEnterNumber() throws Throwable {

		int mStringLength = NUMBER_LENGTH;

		EditText mEditText = (EditText) mActivity
				.findViewById(com.gnl.macambox2.R.id.etPhoneNumberEnterPhoneNumber);

		for (int i = 0; i < mStringLength; i++) {

			Log.i("TEST", "i=" + i);

			this.sendKeys("2 ");

		}

		assertEquals("Text inserted: " + mEditText.getText().toString(),
				mEditText.getText().length(), mStringLength);

	}

	// ------------------------------------------------------------------
	/**Simulates entered number shorter than expected
	 * @throws Throwable
	 */
	public void testEnterNumberTooShort() throws Throwable {

		int mStringLength = NUMBER_LENGTH - 2;

		EditText mEditText = (EditText) mActivity
				.findViewById(com.gnl.macambox2.R.id.etPhoneNumberEnterPhoneNumber);

		for (int i = 0; i < mStringLength; i++) {

			Log.i("TEST", "i=" + i);

			this.sendKeys("2 ");

		}

		assertEquals("Text inserted: " + mEditText.getText().toString(),
				mEditText.getText().length(), mStringLength);

	}

	// ------------------------------------------------------------------
	/**Simulates entered non numeric signs in number string
	 * @throws Throwable
	 */
	public void testEnterNonNumericNumberValue() throws Throwable {

		EditText mEditText = (EditText) mActivity
				.findViewById(com.gnl.macambox2.R.id.etPhoneNumberEnterPhoneNumber);

		this.sendKeys("0 5 - = a f ; , . 3 6 7 ");

		assertEquals("Text inserted: " + mEditText.getText().toString(),
				mEditText.getText().length(), 5);

	}
	// ------------------------------------------------------------------
}
